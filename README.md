# docs

- SAST Documentation: https://docs.gitlab.com/ee/user/application_security/sast/
- SAST report format: https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/sast-report-format.json
- Secret report format: https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/secret-detection-report-format.json

